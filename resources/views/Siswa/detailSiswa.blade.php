@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card" >
            <div class="card-body">
                <h3>Personal Information</h3>
                <form action="" method="post">
                    <div class="mb-3">
                      <label for="" class="form-label">NIS</label>
                      <input type="text"
                        class="form-control" name="" id="" aria-describedby="helpId" value="{{ $data->nis }}" readonly>
                    </div>
                    <div class="mb-3">
                      <label for="" class="form-label">Nama Lengkap</label>
                      <input type="text"
                        class="form-control" name="" id="" aria-describedby="helpId" value="{{ $data->name }}" readonly>
                    </div>
                    <div class="mb-3">
                      <label for="" class="form-label">Pembimbing</label>
                      @if ($data->pembimbing_id == null)
                          <input type="text" class="form-control" name="" id="" aria-describedby="helpId" value="Siswa Belum Memiliki pembimbing" readonly>
                      @else
                          
                      <input type="text"
                        class="form-control" name="" id="" aria-describedby="helpId" value="{{ $data->pembimb }}" readonly>
                      @endif
                    </div>
                    <div class="mb-3">
                      <label for="" class="form-label">Perusahaan Yang Dipilih</label>
                      @if ($data->perusahaan_id == null)
                        <input type="text"class="form-control" name="" id="" aria-describedby="helpId" value="Belum Memilih Perusahaan" readonly>
                      @else 
                        <input type="text" class="form-control" name="" id="" aria-describedby="helpId" value="{{ $data->perusahaan->nama_perusahaan }}" readonly>
                      @endif
                    </div>
                    <div class="mb-3">
                      <label for="" class="form-label">Surat Pengantar PRAKERIN: </label>
                        @if ($data->pengantar_pkl == null)
                            Siswa Belum mengupload Surat pengantar
                        @else
                        <form action="/download" method="post">
                            @csrf
                                <input type="hidden" name="pengantar_pkl" value="{{ $data->pengantar_pkl }}">
                                <input type="submit" class="btn btn-primary" value="Download Pengantar">
                                @endif
                            </div>
                            
                        </form>
                        
                    </div>
                    <div class="mb-3">
                      <label for="" class="form-label">Foto</label>
                      <img width="150px" src="\foto_users\{{ $data->foto_users }}">
                    </div>        
                </div>
                <div class="card" >
                    <div class="card-body">
                    <h3>User Information</h3>
                    <div class="mb-3">
                      <label for="" class="form-label">Username</label>
                      <input type="text"
                        class="form-control" name="" id="" aria-describedby="helpId" value="{{ $data->username }}" readonly>
                    </div>
                    <div class="mb-3">
                      <label for="" class="form-label">Email</label>
                      <input type="email"
                        class="form-control" name="" id="" aria-describedby="helpId" value="{{ $data->email }}" readonly>
                    </div>
                </div>
            </div>
            </form>
<button onclick="kembali()" class="btn btn-danger">Kembali</button>
                <script>function kembali(){
                    window.history.back();
                }</script>        
        @if (Auth()->user()->level == 'siswa')
        @else
            <a href="/editSiswa/{{ $data->id }}" class="btn btn-warning">edit</a>
            <a href="/deleteSiswa/{{ $data->id }}" class="btn btn-danger">delete</a>     
        @endif
    </div>

@endsection